<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppSetting */

$this->title = 'Update App Setting: ' . $model->id_app_setting;
$this->params['breadcrumbs'][] = ['label' => 'App Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_app_setting, 'url' => ['view', 'id' => $model->id_app_setting]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="app-setting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
