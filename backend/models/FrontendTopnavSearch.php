<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\FrontendTopnav;

/**
 * FrontendTopnavSearch represents the model behind the search form of `backend\models\FrontendTopnav`.
 */
class FrontendTopnavSearch extends FrontendTopnav
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_frontend_topnav', 'id_parent_topnav', 'is_expanded', 'is_visible'], 'integer'],
            [['menu_name_lang1', 'menu_name_lang2', 'description_lang1', 'description_lang2', 'link_url', 'file_image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FrontendTopnav::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_frontend_topnav' => $this->id_frontend_topnav,
            'id_parent_topnav' => $this->id_parent_topnav,
            'is_expanded' => $this->is_expanded,
            'is_visible' => $this->is_visible,
        ]);

        $query->andFilterWhere(['like', 'menu_name_lang1', $this->menu_name_lang1])
            ->andFilterWhere(['like', 'menu_name_lang2', $this->menu_name_lang2])
            ->andFilterWhere(['like', 'description_lang1', $this->description_lang1])
            ->andFilterWhere(['like', 'description_lang2', $this->description_lang2])
            ->andFilterWhere(['like', 'link_url', $this->link_url])
            ->andFilterWhere(['like', 'file_image', $this->file_image]);

        return $dataProvider;
    }
}
