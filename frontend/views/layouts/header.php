<?php

use frontend\widgets\topNavWidget;

?>
<style>
    .bg-dark {
        background-color: #5e9517 !important;
    }

    .navbar-brand {
        margin-left: 20px;
    }

</style>
<header>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a style="width: 195px; margin-left: 20px" href=""><img src="" alt="Ecogreen Oleochemicals"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
                <?= topNavWidget::widget(); ?>

    </nav>
</header>
