<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.css',
        'css/style.css',
        'css/carousel.css',
        'css/style_r.css',
        'css/docs.theme.min.css',
        'css/awesome/css/font-awesome.min.css',
        'css/owl.carousel.css',
        'css/owl.theme.default.css',
        'css/balloon.css',
    ];
    public $js = [
        'assets/vendors/jquery.min.js',
        'assets/owlcarousel/owl.carousel.js',
        'js/bootstrap.min.js',
        'js/popper.min.js',
        'js/holder.min.js',
        'js/script_r.js',
        'js/jquery.min.js',
//        'js/owl.carousel.min.js',
        'js/owl.carousel.js',
        'js/tooltip.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
